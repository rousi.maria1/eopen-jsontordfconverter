
package com.application.TwitterService;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

/** Example resource class hosted at the URI path "/myresource"
 */

@Path("/myresource")
public class MyResource {
    
    /** Method processing HTTP GET requests, producing "text/plain" MIME media
     * type.
     * @return String that will be send back as a response of type "text/plain".
     * @throws IOException 
     * @throws JsonIOException 
     * @throws JsonSyntaxException 
     */
	

	    @POST
	    @Path("/post")
	    @Consumes(MediaType.APPLICATION_JSON)
	    public Response createDataInJSON(String data) throws JsonSyntaxException, JsonIOException, IOException { 
	
		String currentLine, allfile="";
		String text=data;
		System.out.println(text);
		
		// Check if input is empty
		if(!text.isEmpty() && isJSONValid(text)) {
					
			// Get selected model and create its model			
			Model selectedmodel = Mapping.buildModel(text);	
					
			// Print the selected model if exists, else print "The model is null!"
			if(selectedmodel!=null) {	
				// Write results in json type in file
				OutputStream out = new FileOutputStream("twitter.ttl");
				Rio.write(selectedmodel, out, RDFFormat.TURTLE);
				out.close();
				    	
				// Read results from file
				File file = new File("twitter.ttl");
				BufferedReader br = new BufferedReader(new FileReader(file));
				while ((currentLine = br.readLine()) != null) {	
					allfile=allfile+currentLine;
					System.out.println(currentLine);
				}
				br.close();
				AddtoRepository.add();
				
				return Response.status(201).entity(allfile).build();
			}
		}	
		
		return Response.status(201).entity("The model is null!").build();
	}
			

	// Check if input is JSON valid
	public boolean isJSONValid(String test) {
	    try {
	    	new JSONArray(test);
	    } catch (JSONException ex) {
	       return false;		        
	    }
		return true;
	}
}