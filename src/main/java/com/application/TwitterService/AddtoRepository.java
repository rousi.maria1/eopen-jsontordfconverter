package com.application.TwitterService;

import java.io.FileInputStream;
import java.io.IOException;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.http.HTTPRepository;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;


public class AddtoRepository {

	public static void add() throws RepositoryException, RDFParseException, IOException {
			
		RepositoryManager repositoryManager = new RemoteRepositoryManager(Server.SERVER);
		repositoryManager.initialize();
		
		Repository twitterRepository = new HTTPRepository(Server.SERVER, Server.REPOSITORY);
		twitterRepository.initialize();
		RepositoryConnection twitterConnection = twitterRepository.getConnection();
		
		loadData(twitterConnection);
				
		twitterConnection.close();

	}
			
	public static void loadData(RepositoryConnection connection) throws RepositoryException, IOException, RDFParseException {
			       
		connection.begin();	
		// Load files in GraphDB repository	
		connection.add(new FileInputStream("twitter.ttl"), "urn:base", RDFFormat.TURTLE);	       
		// Committing the transaction persists the data
		connection.commit();		   
	}
	
	public static void clearData(RepositoryConnection connection) {
		
		// Clear the repository 
        connection.clear();
        connection.remove((Resource) null, null, null);       
	}
}
