package com.application.TwitterService;

import java.io.FileReader;
import java.io.IOException;

import java.io.PrintWriter;

import java.util.ArrayList;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

public class Mapping {
	
	public static Gson gson;
	public static JsonElement json;
	public static JsonObject jobject;
	public static ValueFactory factory;
	public static JsonArray jarray;
	public static String imagecount;
	public static String maskcount;
	public static ArrayList<Location> loc;

	public static Model buildModel(String jsonLine) throws JsonSyntaxException, JsonIOException, IOException {
		
		boolean istop;
		
		int t=0;
		int l=0;
		int labels_sum=1;
		int tweets_sum=1;
		ModelBuilder modelbuilder = new ModelBuilder();
		// Write json input in a file and then read json file
		PrintWriter writer = new PrintWriter("json.json", "UTF-8");
    	writer.println(jsonLine);
    	writer.close();
    	gson = new Gson();
    	json = gson.fromJson(new FileReader("json.json"), JsonElement.class);
    	jarray = json.getAsJsonArray();
		factory = SimpleValueFactory.getInstance();
		
		if(jarray.size()!=0) {
    	   	
    			modelbuilder.setNamespace("as", Namespaces.AS).setNamespace("oa", Namespaces.OA).setNamespace("owl",Namespaces.OWL).setNamespace("rdf", Namespaces.RDF).
				setNamespace("rdfs", Namespaces.RDFS).setNamespace("ta", Namespaces.TA).setNamespace("tweets", Namespaces.TWEETS).setNamespace("xsd", Namespaces.XSD)
				.setNamespace("wgs84_pos", Namespaces.WGS84);
    			modelbuilder.add(Namespaces.TA, RDF.TYPE, OWL.ONTOLOGY);
    		
    			modelbuilder.add(Namespaces.TA, OWL.IMPORTS, factory.createIRI(Namespaces.OA.replace("#", "")));
    			modelbuilder.add(Namespaces.TA, OWL.IMPORTS, factory.createIRI("https://www.w3.org/ns/activitystreams-owl"));
    			modelbuilder.add(Namespaces.TA, OWL.VERSIONINFO, "Created with TopBraid Composer");
    			
    			modelbuilder.add("ta:ClusterBody", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:ClusterBody", RDFS.SUBCLASSOF, "as:Object");
    			
    			modelbuilder.add("ta:Label", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:Label", RDFS.SUBCLASSOF, "as:Object");
    			
    			modelbuilder.add("ta:Tweet", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:Tweet", RDFS.SUBCLASSOF, "as:Note");
    			
    			modelbuilder.add("ta:labels", RDF.TYPE, OWL.OBJECTPROPERTY);
    			modelbuilder.add("ta:labels", RDFS.DOMAIN, "ta:ClusterBody");		
    			modelbuilder.add("ta:labels", RDFS.RANGE, "ta:Label");
    			modelbuilder.add("ta:labels", RDFS.SUBPROPERTYOF, "as:tags");
    			
    			modelbuilder.add("ta:topRanked", RDF.TYPE, OWL.DATATYPEPROPERTY);		
    			modelbuilder.add("ta:topRanked", RDFS.RANGE, "xsd:boolean");
    			
    			int locid=1;
    			for(int j=0; j<jarray.size(); j++) {
    				if(jarray.get(j).getAsJsonObject().has("id") && jarray.get(j).getAsJsonObject().has("labels")  && jarray.get(j).getAsJsonObject().has("tweets") && jarray.get(j).getAsJsonObject().has("top_ranked_tweets")) {
    			 	
	    				modelbuilder.add("ta:Cluster_Annotation_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), RDF.TYPE, "oa:Annotation");
	        			modelbuilder.add("ta:Cluster_Annotation_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "oa:hasBody", "ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString());
	        			modelbuilder.add("ta:Cluster_Annotation_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "oa:hasTarget", "ta:Collection_"+jarray.get(j).getAsJsonObject().get("id").getAsString());
	        			
	        			
	        			for(int i=labels_sum-1;i<jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();i++) {
		    				
		    				modelbuilder.add("ta:Label_"+Integer.valueOf(i+1), RDF.TYPE, "ta:Label");
		        			modelbuilder.add("ta:Label_"+Integer.valueOf(i+1), "as:accuracy", jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().get(i).getAsJsonObject().get("score").getAsFloat());
		        			modelbuilder.add("ta:Label_"+Integer.valueOf(i+1), "as:name", jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().get(i).getAsJsonObject().get("text").getAsString());
		    			}
		    			labels_sum=labels_sum+jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();
		    			
		    			for(int i=tweets_sum-1;i<jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().size();i++) {
		    				
		    				modelbuilder.add("ta:Annotation_"+Integer.valueOf(i+1), RDF.TYPE, "oa:Annotation");
		    				modelbuilder.add("ta:Annotation_"+Integer.valueOf(i+1), "oa:hasBody", "ta:Resource_"+Integer.valueOf(i+1));
		    				modelbuilder.add("ta:Annotation_"+Integer.valueOf(i+1), "oa:hasTarget", factory.createIRI("https://twitter.com/"+jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().get(i).getAsString()));
		    				modelbuilder.add(factory.createIRI("https://twitter.com/"+jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().get(i).getAsString()), RDF.TYPE, "ta:Tweet");
		    			
		    				modelbuilder.add("ta:Resource_"+Integer.valueOf(i+1), RDF.TYPE, RDFS.RESOURCE);
		    			
		        			
		        			istop=false;
		        			for(int k=0;k<jarray.get(j).getAsJsonObject().get("top_ranked_tweets").getAsJsonArray().size();k++) {
		        				if(jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().get(i).getAsString().equals(jarray.get(j).getAsJsonObject().get("top_ranked_tweets").getAsJsonArray().get(k).getAsString())) {
		        					istop=true;
		        					break;
		        				}
		        			}
		        			modelbuilder.add("ta:Resource_"+Integer.valueOf(i+1), "ta:topRanked", istop);
		    			}
		    			tweets_sum=tweets_sum+jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().size();
		    			
		    			modelbuilder.add("ta:Collection_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), RDF.TYPE, "as:Collection");
		    			for(int i=t; i<t+jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().size(); i++) {
		    				
		    				modelbuilder.add("ta:Collection_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "as:items", "ta:Annotation_"+Integer.valueOf(i+1));
		    			}
		    			
		    			modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), RDF.TYPE, "ta:ClusterBody");
		    			for(int i=l;i<l+jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();i++) {
		    				
		    				modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "ta:labels", "ta:Label_"+Integer.valueOf(i+1));
		    				if(jarray.get(j).getAsJsonObject().has("location")) {

			        			modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "wgs84_pos:lat", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("latitude").getAsString()));		
			        			modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "wgs84_pos:long", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("longitude").getAsString()));
			    				locid++;
			    			}
		    			}	
		    			l=labels_sum-1;
		    			t=tweets_sum-1;	
    				}	    	
    			}
		    	Model model = modelbuilder.build();
		    	return model;
    		}		
    	return null;
    }

}
